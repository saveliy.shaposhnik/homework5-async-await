const btn = document.getElementById("find-ip"),
URI = "https://api.ipify.org/?format=json";

btn.addEventListener("click",function(){
    getIp().then((data) => {
        console.log(data.ip);
        getPersone(data.ip).then(({regionName,country,region,city}) => {
            const regionNameP = document.createElement("p");
            const countryP = document.createElement("p");
            const regionP = document.createElement("p");
            const cityP = document.createElement("p");

            regionNameP.textContent = `Region name:${regionName}`;
            countryP.textContent = `Country:${country}`;
            regionP.textContent = `Region:${region}`;
            cityP.textContent = `City:${city}`;

            btn.after(regionNameP,countryP,regionP,cityP);

        })
    })
})

async function getIp() {
    return await fetch(URI)
      .then((response) => { 
          return response.json(); 
        })
      .catch((error) => { console.log(error); });
}
  
  async function getPersone(ip) {
    return await fetch(`http://ip-api.com/json/${ip}`)
      .then((response) => { return response.json(); })
      .catch((err) => { console.log(err); });
}

  